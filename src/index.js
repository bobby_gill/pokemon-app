import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import fetch from 'unfetch';
import { InMemoryCache } from 'apollo-cache-inmemory';
import 'react-app-polyfill/stable';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './components/App';

const httpLink = new HttpLink({
    uri: 'https://pokemon-samdavies.stylindex.now.sh',
    fetch: fetch,
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>,
    document.getElementById('root')
);
