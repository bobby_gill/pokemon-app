import styled from 'styled-components';

const Wrapper = styled.div.attrs({ className: 'container-fluid' })`
    margin: 30px 0;
`;

const Item = styled.div`
    position: relative;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    width: 100%;
    max-width: 600px;
    border: 1px solid #ccc;
    border-radius: 8px;
    overflow: hidden;
    margin: 15px auto;
    padding: 15px;
`;

const ImageWrapper = styled.div`
    flex: 0 100px;
    min-width: 60px;
    margin-right: 15px;
    margin-bottom: 5px;
    
    img {
        width: 100%;
        height: auto;
    }
`;

const InfoWrapper = styled.div`
    display: flex;
    flex: 1 auto;
    min-width: 260px;
    margin-bottom: 5px;
`;

const LeftWrapper = styled.div`
    flex: 1;
    max-width: 50%;
    padding-right: 5px;
`;

const RightWrapper = styled.div`
    flex: 1;
    max-width: 50%;
    padding-left: 5px;
`;

const ItemRow = styled.div`
    display: flex;
    
    label {
        color: #aaa;
        min-width: 70px;
        width: 50%;
    }
    span {
        display: inline-block;
        width: calc(100% - 70px);
        max-width:: 50%;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
`;

const Label = styled.h4`
    margin: 15px;
    padding: 15px;
    text-align: center;
`;

const MoreButton = styled.div `
    position: absolute;
    bottom: 0;
    right: 15px;
    
    a {
        color: #ffca00;
        text-decoration: none;
        
        &:hover {
            color: #dfb000;
        }
    }
`;

export default {
    Wrapper,
    ImageWrapper,
    InfoWrapper,
    LeftWrapper,
    RightWrapper,
    ItemRow,
    Item,
    Label,
    MoreButton,
};
