import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo';
import { Row, Col, Button } from 'reactstrap';

import Styled from './styled';
import query from './query';

class Detail extends Component {
    render() {
        const { match: { params: { id } } } = this.props;

        return (
            <Query query={query(id)}>
                {({data, loading}) => {
                    if (loading || !data)
                        return (
                            <Styled.Label>
                                Loading ...
                            </Styled.Label>
                        );

                    const { pokemon } = data;

                    return (
                        <Styled.Wrapper>
                            <Row>
                                <Col xs={12} className="text-center">
                                    <img src={pokemon['image']} alt={pokemon['image']} style={{maxWidth: '100%'}}/>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>ID: </label>
                                        <span>{pokemon['id']}</span>
                                    </Styled.ItemRow>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>Number: </label>
                                        <span>{pokemon['number']}</span>
                                    </Styled.ItemRow>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>Name: </label>
                                        <span>{pokemon['name']}</span>
                                    </Styled.ItemRow>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>Types: </label>
                                        <span>{pokemon['types'].join(', ')}</span>
                                    </Styled.ItemRow>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>Max HP: </label>
                                        <span>{pokemon['maxHP']}</span>
                                    </Styled.ItemRow>
                                </Col>
                                <Col md={4} sm={6}>
                                    <Styled.ItemRow>
                                        <label>Max CP: </label>
                                        <span>{pokemon['maxCP']}</span>
                                    </Styled.ItemRow>
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <Col xs={12}>
                                    <h4 className="text-center">
                                        {pokemon['evolutions'].length > 0 ? 'Evolutions' : 'No Evolution'}
                                    </h4>
                                    {pokemon['evolutions'].map(item => {
                                        return (
                                            <Styled.Item key={item.id}>
                                                <Styled.ImageWrapper>
                                                    <img src={item['image']} alt={item['image']} />
                                                </Styled.ImageWrapper>
                                                <Styled.InfoWrapper>
                                                    <Styled.LeftWrapper>
                                                        <Styled.ItemRow>
                                                            <label>ID: </label>
                                                            <span>{item['id']}</span>
                                                        </Styled.ItemRow>
                                                        <Styled.ItemRow>
                                                            <label>Number: </label>
                                                            <span>{item['number']}</span>
                                                        </Styled.ItemRow>
                                                        <Styled.ItemRow>
                                                            <label>maxCP: </label>
                                                            <span>{item['maxCP']}</span>
                                                        </Styled.ItemRow>
                                                    </Styled.LeftWrapper>
                                                    <Styled.RightWrapper>
                                                        <Styled.ItemRow>
                                                            <label>Name: </label>
                                                            <span>{item['name']}</span>
                                                        </Styled.ItemRow>
                                                        <Styled.ItemRow>
                                                            <label>Types: </label>
                                                            <span>{item['types'].join(', ')}</span>
                                                        </Styled.ItemRow>
                                                        <Styled.ItemRow>
                                                            <label>maxHP: </label>
                                                            <span>{item['maxHP']}</span>
                                                        </Styled.ItemRow>
                                                    </Styled.RightWrapper>
                                                </Styled.InfoWrapper>
                                                <Styled.MoreButton>
                                                    <Link to={`/${item['id']}`}>More</Link>
                                                </Styled.MoreButton>
                                            </Styled.Item>
                                        );
                                    })}
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <Col xs={12} className="text-center">
                                    <Button onClick={this.props.history.goBack}>Back</Button>
                                </Col>
                            </Row>
                        </Styled.Wrapper>
                    );
                }}
            </Query>
        );
    }
}

export default Detail;
