import gql from "graphql-tag";

export default (id) => {
    return gql`
      {
        pokemon(id: "${id}") {
          id
          number
          name
          maxCP
          maxHP
          image
          types
          evolutions {
            id
            number
            name
            maxCP
            maxHP
            image
            types
          }
        }
      }
    `;
}
