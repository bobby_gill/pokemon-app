import React from 'react';
import { MockedProvider } from '@apollo/react-testing';
import { act } from 'react-dom/test-utils';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Row } from 'reactstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import waitForExpect from 'wait-for-expect';

import query from './query';
import Detail from './index';
import Styled from './styled';

Enzyme.configure({ adapter: new Adapter() });

describe("Tests for Details Component", () => {
    const mocks = [
        {
            request: {
                query: query("UG9rZW1vbjowMDE=")
            },
            result: {
                data: {
                    "pokemon": {
                        "id": "UG9rZW1vbjowMDE=",
                        "number": "001",
                        "name": "Bulbasaur",
                        "maxCP": 951,
                        "maxHP": 1071,
                        "image": "https://img.pokemondb.net/artwork/bulbasaur.jpg",
                        "types": [
                            "Grass",
                            "Poison"
                        ],
                        "evolutions": [
                            {
                                "id": "UG9rZW1vbjowMDI=",
                                "number": "002",
                                "name": "Ivysaur",
                                "maxCP": 1483,
                                "maxHP": 1632,
                                "image": "https://img.pokemondb.net/artwork/ivysaur.jpg",
                                "types": [
                                    "Grass",
                                    "Poison"
                                ]
                            },
                            {
                                "id": "UG9rZW1vbjowMDM=",
                                "number": "003",
                                "name": "Venusaur",
                                "maxCP": 2392,
                                "maxHP": 2580,
                                "image": "https://img.pokemondb.net/artwork/venusaur.jpg",
                                "types": [
                                    "Grass",
                                    "Poison"
                                ]
                            }
                        ]
                    }
                },
            },
        },
    ];

    it('should render loading state initially', async () => {
        let wrapper;
        act(() => {
            wrapper = mount(
                <MockedProvider mocks={[]} addTypename={false}>
                    <Router>
                        <Detail
                            match={{params: {id: "UG9rZW1vbjowMDE="}}}
                            history={{}}
                        />
                    </Router>
                </MockedProvider>
            );
        });

        await act(async () => {
            await waitForExpect(() => {
                wrapper.update();
            });
        });
        expect(wrapper.find(Styled.Label).text()).toEqual('Loading ...');
    });

    it('should render component without error', async () => {
        const wrapper = mount(
            <MockedProvider mocks={mocks} addTypename={false}>
                <Router>
                    <Detail
                        match={{params: {id: "UG9rZW1vbjowMDE="}}}
                        history={{}}
                    />
                </Router>
            </MockedProvider>
        );

        await act(async () => {
            await waitForExpect(() => {
                wrapper.update();
            });
        });
        expect(wrapper.find('div.col-md-4')).toHaveLength(6);
        expect(wrapper.find(Styled.Item)).toHaveLength(2);
    });
});
