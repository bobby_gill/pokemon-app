import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import List from './List';
import Detail from './Detail';

class App extends Component {
  render() {
    return (
        <Router>
          <React.Fragment>
              <Switch>
                <Route exact path={'/'} component={List}/>
                <Route exact path={'/:id'} component={Detail}/>
              </Switch>
          </React.Fragment>
        </Router>
    );
  }
}

export default App;
