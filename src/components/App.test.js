import React from 'react';
import ReactDom from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import App from './App';

const httpLink = new HttpLink({
    uri: 'https://pokemon-samdavies.stylindex.now.sh',
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
});

it('App renders without crashing', () => {
    const div = document.createElement('div');
    ReactDom.render(
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>, div);
});
