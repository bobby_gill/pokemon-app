import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Query } from 'react-apollo';

import Styled from './styled';
import query from './query';

class List extends Component {
    render() {
        return (
            <Query query={query}>
                {({data, loading}) => {
                    if (loading || !data)
                        return (
                            <Styled.Label>
                                Loading ...
                            </Styled.Label>
                        );

                    const { pokemons } = data;

                    if (pokemons.length === 0)
                        return (
                            <Styled.Label>
                                Empty Data
                            </Styled.Label>
                        );

                    return (
                        <Styled.Wrapper>
                            {pokemons.map(item => {
                                return (
                                    <Styled.Item key={item.id}>
                                        <Styled.ImageWrapper>
                                            <img src={item['image']} alt={item['image']} />
                                        </Styled.ImageWrapper>
                                        <Styled.InfoWrapper>
                                            <Styled.LeftWrapper>
                                                <Styled.ItemRow>
                                                    <label>ID: </label>
                                                    <span>{item['id']}</span>
                                                </Styled.ItemRow>
                                                <Styled.ItemRow>
                                                    <label>Number: </label>
                                                    <span>{item['number']}</span>
                                                </Styled.ItemRow>
                                                <Styled.ItemRow>
                                                    <label>maxCP: </label>
                                                    <span>{item['maxCP']}</span>
                                                </Styled.ItemRow>
                                            </Styled.LeftWrapper>
                                            <Styled.RightWrapper>
                                                <Styled.ItemRow>
                                                    <label>Name: </label>
                                                    <span>{item['name']}</span>
                                                </Styled.ItemRow>
                                                <Styled.ItemRow>
                                                    <label>Types: </label>
                                                    <span>{item['types'].join(', ')}</span>
                                                </Styled.ItemRow>
                                                <Styled.ItemRow>
                                                    <label>maxHP: </label>
                                                    <span>{item['maxHP']}</span>
                                                </Styled.ItemRow>
                                            </Styled.RightWrapper>
                                        </Styled.InfoWrapper>
                                        <Styled.MoreButton>
                                            <Link to={`/${item['id']}`}>More</Link>
                                        </Styled.MoreButton>
                                    </Styled.Item>
                                );
                            })}
                        </Styled.Wrapper>
                    );
                }}
            </Query>
        );
    }
}

export default List;
