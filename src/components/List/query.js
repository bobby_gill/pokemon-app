import gql from "graphql-tag";

export default gql`
  {
    pokemons(first: 10) {
      id
      number
      name
      maxCP
      maxHP
      image
      types
    }
  }
`;
